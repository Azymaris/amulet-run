horse v1.0
Based on "LPC Horses" by bluecarrot16: https://opengameart.org/content/lpc-horses
Reworked for Stendhal by Jordan Irwin (AntumDeluge)
Licensing:
  Only license text for Creative Commons Attribution-ShareAlike 3.0 (CC BY-SA 3.0)
  is included with this release (see: LICENSE.txt). But this work may also be
  released/redistributed under Creative Commons Attribution version 3.0 (CC BY 3.0),
  or GNU General Public License version 2.0 (GPL 2.0) or version 3.0 (GPL 3.0)
  according to the original author.

2D Fantasy-based quest game.

Much of the artwork comes from various creators on the OpenGameArt community:
Search for the files on https://opengameart.org/

The music comes from:
https://opengameart.org/content/orchestral-battle-music


extends Node2D


var ghost = preload("res://scenes/ghost.tscn")
onready var HUD = get_node("HUD")
onready var player = get_node("player")

func _ready():
	randomize()
	set_process(true)
	HUD.show_message("Amulet Run")
	
func _process(delta):
	HUD.update()
	
func spawn_ghost():
	var a = ghost.instance()
	add_child(a)
	var size = get_node("ghost").get_size()
	var pos = Vector2(50, 50)
	a.init(size, pos, Vector2(0,0))






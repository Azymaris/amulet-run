extends Node

# game settings
var game_over = false
var score = 0
var level = 0
var paused = false
var current_scene = null
var new_scene = null

# player settings
var shield_max = 100
var shield_regen = 10
var fireball_damage = 10
var shield_level = shield_max

# NPC timers
var sheep_timer = 15
var brown_horse_timer = 15
var white_horse_timer = 15
var ghost_timer = 15
var eyeball_timer = 15
var egg_timer = 15
var cow_timer = 15
var chicken_timer = 15
var zombie_timer = 15
var grue_timer = 15
var skeleton_timer = 15

# enemy settings
var ghost_bullet_damage = 25
var ghost_health = 30
var ghost_points = 100

var egg_bullet_damage = 25
var egg_health = 30
var egg_points = 100

var eyeball_bullet_damage = 25
var eyeball_health = 30
var eyeball_points = 100

var grue_bullet_damage = 25
var grue_health = 30
var grue_points = 100

var skeleton_bullet_damage = 25
var skeleton_health = 30
var skeleton_points = 100

var zombie_bullet_damage = 25
var zombie_health = 30
var zombie_points = 100


# asteroid settings
var break_pattern = {'big': 'med',
					 'med': 'sm',
					 'sm': 'tiny',
					 'tiny': null}

var ast_damage = {'big': 40,
			  	  'med': 20,
		      	  'sm': 15,
			  	  'tiny': 10}

var ast_points = {'big': 10,
			  	  'med': 15,
		      	  'sm': 20,
			  	  'tiny': 40}

func _ready():
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() - 1)

func goto_scene(path):
	var s = ResourceLoader.load(path)
	new_scene = s.instance()
	get_tree().get_root().add_child(new_scene)
	get_tree().set_current_scene(new_scene)
	current_scene.queue_free()
	current_scene = new_scene

func new_game():
	game_over = false
	score = 0
	level = 0
	goto_scene("res://scenes/main.tscn")
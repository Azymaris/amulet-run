extends KinematicBody2D

onready var effect = get_node("effect")
onready var sprite = get_node("sprite")

var fire_scene = preload("res://scenes/fire.tscn")
signal explode
var health = global.ghost_health
var points = global.ghost_points

var vel=Vector2()
var pos = Vector2()
var magnitude = 3
var movement = 0

func _ready():
	randomize()
	get_node("timer").set_wait_time(global.ghost_timer)
	pos = get_pos()
	add_to_group("enemies")
	effect.interpolate_property(sprite, "transform/scale", sprite.get_scale(), Vector2(2,2), 0.3, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	effect.interpolate_property(sprite, "visibility/opacity", 1, 0.5, 0.3, Tween.TRANS_BOUNCE, Tween.EASE_OUT)	
	set_process(true)
	
func _process(delta):
	movement = magnitude*vel*delta
	pos += movement
	set_pos(pos + movement)

func _on_timer_timeout():
	var x_vel = rand_range(-5, 5)
	var y_vel = rand_range(-5, 5)
	vel=Vector2(x_vel, y_vel)
	get_node("sound").play("ghost")

func _on_visibility_enabler_enter_screen():
	get_node("timer").start()

func _on_visibility_enabler_exit_screen():
	get_node("timer").stop()

func damage(amount):
	health -= amount
	if health <= 0:
		connect("explode", self, "explode")
		emit_signal("explode", get_global_pos())

func explode(pos):
	effect.start()

func _on_effect_tween_complete( object, key ):
	var fire = fire_scene.instance()
	fire.set_pos(pos)
	get_parent().add_child(fire)
	global.score += points
	queue_free()
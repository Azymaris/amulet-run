extends KinematicBody2D

var vel=Vector2()
var pos = Vector2()
var magnitude = 3
var movement = 0

func _ready():
	randomize()
	pos = get_pos()
	add_to_group("enemies")
	set_process(true)
	
func _process(delta):
	movement = magnitude*vel*delta
	if abs(vel.x) > abs (vel.y):
		if vel.x <  0:
			get_node("animated_sprite").set_animation("walk_left")
			get_node("animated_sprite").play()
		elif vel.x > 0:
			get_node("animated_sprite").set_animation("walk_right")
			get_node("animated_sprite").play()
	else:
		if vel.y < 0:
			get_node("animated_sprite").set_animation("walk_up")
			get_node("animated_sprite").play()
		elif vel.y > 0:
			get_node("animated_sprite").set_animation("walk_down")
			get_node("animated_sprite").play()

	pos += movement
	set_pos(pos + movement)

func _on_timer_timeout():
	var x_vel = rand_range(-5, 5)
	var y_vel = rand_range(-5, 5)
	vel=Vector2(x_vel, y_vel)
	if rand_range(-1, 1) > 0:
		get_node("sound").play("sheep1")
	else:
		get_node("sound").play("sheep2")


func _on_VisibilityEnabler2D_enter_screen():
	get_node("timer").start()


func _on_VisibilityEnabler2D_exit_screen():
	get_node("timer").stop()

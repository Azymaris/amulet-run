extends Node2D

func _ready():
	get_node("timer").start()
	get_node("animated_sprite").play()
	get_node("sound").play("burning")

func _on_timer_timeout():
	queue_free()
